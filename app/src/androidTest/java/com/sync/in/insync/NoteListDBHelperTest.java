package com.sync.in.insync;

import android.content.ContentValues;
import android.support.test.InstrumentationRegistry;

import com.sync.in.insync.data.NoteListContract;
import com.sync.in.insync.data.NoteListDbHelper;
import com.sync.in.insync.utils.StringUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by eyoel on 6/22/18.
 */
public class NoteListDBHelperTest {
    private NoteListDbHelper dbHelper;
    private NoteListContract.NoteListEntry noteListEntry;
    private AddNoteActivity addNoteActivity;
    private StringUtil stringUtil;

    @Before
    public void setUp(){
        dbHelper = new NoteListDbHelper(InstrumentationRegistry.getTargetContext());
        dbHelper.getWritableDatabase();
    }

    @After
    public void finish(){
        dbHelper.close();
    }

    @Test
    public void testAddNote(){
        String title = "TestNote" , body = "Test Note Body goes here", excerpt = StringUtil.getExcerpt(body), rmid = "asdf";
        ContentValues cv = new ContentValues();
        cv.put(NoteListContract.NoteListEntry.COLUMN_TITLE, title);
        cv.put(NoteListContract.NoteListEntry.COLUMN_BODY, body);
        cv.put(NoteListContract.NoteListEntry.COLUMN_EXCERPT, StringUtil.getExcerpt(body));
        Assert.assertEquals("Must be the same", cv, dbHelper.addNote(title, body));
    }

    @Test
    public  void testAddSyncedNote(){
        String title = "TestNote" , body = "Test Note Body goes here", excerpt = StringUtil.getExcerpt(body), rmid = "asdf";
        ContentValues contentValues = new ContentValues();
        contentValues.put(NoteListContract.NoteListEntry.COLUMN_TITLE, title);
        contentValues.put(NoteListContract.NoteListEntry.COLUMN_BODY, body);
        contentValues.put(NoteListContract.NoteListEntry.COLUMN_EXCERPT, excerpt);
        contentValues.put(NoteListContract.NoteListEntry.REMOTE_ID, rmid);
        contentValues.put(NoteListContract.NoteListEntry.STATUS, NoteListDbHelper.NoteStatus.SYNCED);
        Assert.assertEquals("Must be the same", contentValues, dbHelper.addSyncedNote(title, body, excerpt,rmid));
    }

    @Test
    public void testUpdateNote(){
        String title = "TestNote" , body = "Test Note Body goes here", excerpt = StringUtil.getExcerpt(body), rmid = "asdf";
        ContentValues cv = new ContentValues();
        cv.put(NoteListContract.NoteListEntry.COLUMN_TITLE, title);
        cv.put(NoteListContract.NoteListEntry.COLUMN_BODY, body);
        cv.put(NoteListContract.NoteListEntry.COLUMN_EXCERPT, excerpt);
        Assert.assertEquals("Must be the same", cv, dbHelper.updateNote(title, body,excerpt));
    }

    @Test
    public void testDeleteNoteById(){
        ContentValues cv = new ContentValues();
        cv.put(NoteListContract.NoteListEntry.STATUS, NoteListDbHelper.NoteStatus.DELETED);
        boolean expected = dbHelper.getWritableDatabase().update(NoteListContract.NoteListEntry.TABLE_NAME,
                cv,
                NoteListContract.NoteListEntry._ID + "=?", new String[]{String.valueOf(10)}) > 0;
        Assert.assertEquals("Must be the same", expected, NoteListDbHelper.deleteNoteById(String.valueOf(10),dbHelper.getWritableDatabase()));

    }
}
