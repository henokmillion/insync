package com.sync.in.insync;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.sync.in.insync.data.NoteListContract;
import com.sync.in.insync.data.NoteListDbHelper;
import com.sync.in.insync.utils.AuthStateHandler;
import com.sync.in.insync.utils.JobScheduleUtil;

public class AddNoteActivity extends AppCompatActivity {

    EditText title, body;
    SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        AuthStateHandler authStateHandler = new AuthStateHandler(this);

        if (!authStateHandler.isAuthenticated()) {
            Intent startLoginActivity = new Intent(this, LoginActivity.class);
            startActivity(startLoginActivity);
        }

        title = (EditText) findViewById(R.id.add_note_et_title);
        body = (EditText) findViewById(R.id.add_note_et_body);
        NoteListDbHelper dbHelper = new NoteListDbHelper(this);
        mDb = dbHelper.getWritableDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_note_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case R.id.add_note_action_save:
                saveNote();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean saveNote() {
        if (!addNote(title.getText(), body.getText())) {
            Toast.makeText(this, "note not added", Toast.LENGTH_SHORT).show();
        } else {
            JobScheduleUtil.scheduleJob(this);
        }
        Intent startMainActivity = new Intent(this, MainActivity.class);
        startActivity(startMainActivity);
        return true;
    }

    private boolean addNote(Editable titleText, Editable bodyText) {
        return mDb.insert(NoteListContract.NoteListEntry.TABLE_NAME, null,
                NoteListDbHelper.addNote(titleText.toString(), bodyText.toString())) > 0;
    }

    private boolean addNote(String titleText, String bodyText) {
        return mDb.insert(NoteListContract.NoteListEntry.TABLE_NAME, null,
                NoteListDbHelper.addNote(titleText, bodyText)) > 0;
    }
}
