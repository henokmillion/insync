package com.sync.in.insync;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sync.in.insync.data.NoteListContract;
import com.sync.in.insync.data.NoteListDbHelper;
import com.sync.in.insync.utils.AuthStateHandler;
import com.sync.in.insync.utils.JobScheduleUtil;

public class EditNoteActivity extends AppCompatActivity {

    SQLiteDatabase mDb;
    Cursor mCursor;
    EditText titleEditText, bodyEditText;
    String mId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        AuthStateHandler authStateHandler = new AuthStateHandler(this);

        if (!authStateHandler.isAuthenticated()) {
            Intent startLoginActivity = new Intent(this, LoginActivity.class);
            startActivity(startLoginActivity);
        }

        mId = getIntent().getStringExtra("_id");
        int position = getIntent().getIntExtra("position", 0);
        Toast.makeText(getBaseContext(), mId, Toast.LENGTH_LONG).show();
        NoteListDbHelper noteListDbHelper = new NoteListDbHelper(this);
        mDb = noteListDbHelper.getWritableDatabase();
//        mCursor.moveToPosition(position);
        mCursor = getNoteById(mId);
        if (mCursor != null) {
            Log.i("EDIT NOTE", "cursor: "+mCursor.moveToFirst());
        }

        titleEditText = (EditText) findViewById(R.id.edit_note_title_edit_text);
        bodyEditText = (EditText) findViewById(R.id.edit_note_body_edit_text);
        if (!mCursor.moveToFirst()) {
            Log.i("EDIT NOTE", "--position: "+position + "id"+mId);
            return;
        }

        Log.i("EDIT NOTE: ", "position: "+position + "id"+mId);
        titleEditText.setText(mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_TITLE)));
        bodyEditText.setText(mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_EXCERPT)));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_note_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.edit_note_action_cancel:
                Intent startMainActivity = new Intent(this, MainActivity.class);
                startActivity(startMainActivity);
                break;
            case R.id.edit_note_action_save:
                Toast.makeText(this, "note saved", Toast.LENGTH_SHORT).show();
                saveNote();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveNote() {
        updateNote(titleEditText.getText(), bodyEditText.getText(), mId);
        Toast.makeText(this, "note saved", Toast.LENGTH_SHORT).show();
        Intent startMainActivity = new Intent(this, MainActivity.class);
        startActivity(startMainActivity);
    }

    private long updateNote(Editable title, Editable body, String id) {
        JobScheduleUtil.scheduleJob(getBaseContext());
        return mDb.update(NoteListContract.NoteListEntry.TABLE_NAME,
                NoteListDbHelper.updateNote(title,
                body, ""), "_id=" + id, null);
    }

    private Cursor getNoteById(String id) {
        return mDb.rawQuery("select * from " + NoteListContract.NoteListEntry.TABLE_NAME +
                " where " + NoteListContract.NoteListEntry._ID+ "='" + id + "'" , null);
    }

    private long updateNote(String title, String excerpt, String _id) {
        return mDb.update(NoteListContract.NoteListEntry.TABLE_NAME, NoteListDbHelper.updateNote(title,
                "", excerpt), "_id=" + _id, null);
    }

}
