package com.sync.in.insync;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.sync.in.insync.data.NoteListContract;
import com.sync.in.insync.data.NoteListDbHelper;
import com.sync.in.insync.utils.AuthStateHandler;
import com.sync.in.insync.utils.JobScheduleUtil;
import com.sync.in.insync.utils.NetworkJobService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private View v;
    @SuppressLint("StaticFieldLeak")
    public static RecyclerView mRecyclerView;
    private List<Note> lstNotes;
    private boolean mTwoPane = false;
    private DetailFragment.OnFragmentInteractionListener fragmentInteractionListener;
    private SQLiteDatabase mDb;
    private Cursor mCursor;
    private static final String TAG = "MainActivity: ";
    private static RecyclerViewAdapter rvAdapter;
//    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // check auth state, start login activity if false
        AuthStateHandler authStateHandler = new AuthStateHandler(this);
        if (!authStateHandler.isAuthenticated()) {
            Intent startLoginActivity = new Intent(this, LoginActivity.class);
            startActivity(startLoginActivity);
        }

        if (connectionAlive()) {
            Toast.makeText(this, "connection is active", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "connection is active");
            scheduleJob(getBaseContext());
        } else {
            Toast.makeText(this, "no internet connection", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "no internet connection");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addNewNoteIntent = new Intent(getBaseContext(), AddNoteActivity.class);
                PendingIntent pendingIntent = TaskStackBuilder.create(getBaseContext())
                        .addNextIntentWithParentStack(addNewNoteIntent)
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(getBaseContext());
                builder.setContentIntent(pendingIntent);
                startActivity(addNewNoteIntent);
            }
        });

        fragmentInteractionListener = new DetailFragment.OnFragmentInteractionListener() {
            @Override
            public void onFragmentInteraction(Uri uri) {
            }
        };

//        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//
//            @Override
//            public void onRefresh() {
//                Toast.makeText(getBaseContext(), "refreshing...", Toast.LENGTH_LONG).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (findViewById(R.id.note_detail_container) != null) {
            mTwoPane = true;
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.note_detail_container, new DetailFragment())
                        .commit();
            }
        } else {
            mTwoPane = false;
        }

        NoteListDbHelper noteListDbHelper = new NoteListDbHelper(this);
        mDb = noteListDbHelper.getWritableDatabase();
        mCursor = getAllNotes();
        mRecyclerView = (RecyclerView) findViewById(R.id.home_rv);
//        mRecyclerView.setHasFixedSize(true);

        rvAdapter = new RecyclerViewAdapter(getBaseContext(), mCursor);
        rvAdapter.swapCursor(getAllNotes());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mRecyclerView.setAdapter(rvAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                viewHolder.itemView.setBackgroundColor(0);
                mCursor = getAllNotes();
                mCursor.moveToPosition(viewHolder.getAdapterPosition());
                String noteID = mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry._ID));
                Toast.makeText(getBaseContext(),"Note [" + noteID +"] Is going to be deleted.", Toast.LENGTH_SHORT).show();
                deleteNote(noteID);
                JobScheduleUtil.scheduleJob(getBaseContext());
                rvAdapter.swapCursor(getAllNotes());
                rvAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        }).attachToRecyclerView(mRecyclerView);


//        FetchDataTask fetchDataTask = new FetchDataTask();
//        try {
//            fetchDataTask.execute(new URL("http://10.0.2.2:3000/api/v1/note"));
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
    }

    public class FetchDataTask extends AsyncTask<URL, Void, String> {
        @Override
        protected String doInBackground(URL... params) {
            String data = "";
            String page = "";
            URL url = params[0];
            HttpURLConnection conn;
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.connect();

                InputStream is = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                while((data = reader.readLine()) != null) {
                    page += data + "\n";
                }
                return page;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return page;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null && !s.equals("")) {
//                Toast.makeText(getBaseContext(), s, Toast.LENGTH_LONG).show();
            }
            Log.d("DATA DOWNLOADED: ", s);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent startSettingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(startSettingsActivity);
            return true;
        }
        if (id == R.id.action_refresh) {
//            mSwipeRefreshLayout.setRefreshing(true);
            scheduleJob(getBaseContext());
        }
        if (id == R.id.action_logout) {
            logout();
        }



        return super.onOptionsItemSelected(item);
    }

    private static void swapCursor(Cursor cursor) {
        rvAdapter.swapCursor(cursor);
    }

    /**
     * logout
     */
    @SuppressLint("CommitPrefEdits")
    public void logout() {
        //TODO: change auth state in SharedPreferences
        //TODO: clean sqlite database

        //show login screen

        getSharedPreferences(AuthStateHandler.SHARED_PREFERENCES, MODE_PRIVATE).edit()
                .putBoolean(AuthStateHandler.AUTH_STATE, false)
                .putString(AuthStateHandler.AUTH_TOKEN, null);

        Intent startLoginActivity = new Intent(getBaseContext(), LoginActivity.class);
        startActivity(startLoginActivity);
        NoteListDbHelper noteListDbHelper = new NoteListDbHelper(getBaseContext());
        noteListDbHelper.onUpgrade(mDb, 1, 1);
    }

    /**
     * fetchData
     * fetch data from an api
     * @param url
     * @return
     * @throws IOException
     */
    public String fetchData(URL url) throws IOException {
//        String notesAddress = "https://www.google.com.et";
//        URL notesUrl = new URL(notesAddress);
        return "";
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id) {
            // handle nav drawer actions
            case R.id.nav_manage:
                Intent addNoteActivity = new Intent(this, AddNoteActivity.class);
                startActivity(addNoteActivity);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Cursor getAllNotes() {
        return mDb.query(
                NoteListContract.NoteListEntry.TABLE_NAME,
                null, NoteListContract.NoteListEntry.STATUS+" NOT LIKE '"+
                NoteListDbHelper.NoteStatus.DELETED +"'", null, null, null, null
        );
    }

    private long addNote(String title, String excerpt) {
        return mDb.insert(NoteListContract.NoteListEntry.TABLE_NAME, null,
                NoteListDbHelper.addNote(title, excerpt));
    }


    @Override
    protected void onPause() {
        mCursor = getAllNotes();
        rvAdapter.swapCursor(mCursor);
        rvAdapter.notifyDataSetChanged();
        rvAdapter.notifyDataSetChanged();
        super.onPause();
    }



    /**
     * delete notes
     * delete notes in sqlitedb
     * @param id
     * @return
     */
    private boolean deleteNote(String id) {
//        return mDb.delete(NoteListContract.NoteListEntry.TABLE_NAME,
//                NoteListContract.NoteListEntry._ID  +"=" +id, null) > 0;
        return NoteListDbHelper.deleteNoteById(id, mDb);
    }

    private void scheduleJob(Context baseContext) {
        JobScheduleUtil.scheduleJob(baseContext);
    }

    public boolean connectionAlive() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        } else {
            return false;
        }
        return networkInfo != null && networkInfo.isConnected();
    }

    public static class FetchFromDB extends AsyncTask<Void, Void, Void> {

        Context mContext;
        Cursor mCursor;

        public FetchFromDB(Context context) {
            this.mContext = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            NoteListDbHelper dbHelper = new NoteListDbHelper(mContext);
            SQLiteDatabase mDb = dbHelper.getWritableDatabase();
            mCursor = mDb.query(
                    NoteListContract.NoteListEntry.TABLE_NAME,
                    null, NoteListContract.NoteListEntry.STATUS+" NOT LIKE '"+
                            NoteListDbHelper.NoteStatus.DELETED +"'", null, null, null, null
            );
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.i(TAG, "SWAPPING CURSOR");
            MainActivity.rvAdapter.swapCursor(this.mCursor);
        }
    }

}
