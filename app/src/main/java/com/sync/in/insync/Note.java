package com.sync.in.insync;

/**
 * Created by User on 5/18/2018.
 */

public class Note {
    private String title;
    private String excerpt;
    private String body;

    public Note(String title, String excerpt, String body) {
        this.title = title;
        this.excerpt = excerpt;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
