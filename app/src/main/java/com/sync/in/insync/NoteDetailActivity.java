package com.sync.in.insync;

import android.app.PendingIntent;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sync.in.insync.data.NoteListContract;
import com.sync.in.insync.data.NoteListDbHelper;
import com.sync.in.insync.utils.AuthStateHandler;
import com.sync.in.insync.utils.JobScheduleUtil;

public class NoteDetailActivity extends AppCompatActivity {

    String title, body, _id;
    SQLiteDatabase mDb;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_detail_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        NoteListDbHelper dbHelper = new NoteListDbHelper(this);
        mDb = dbHelper.getWritableDatabase();


        //noinspection SimplifiableIfStatement
        if (id == R.id.item_detail_edit_note) {
            Intent startEditNoteActivity = new Intent(this, EditNoteActivity.class);
            startEditNoteActivity.putExtra("_id", _id);
//            startActivity(startEditNoteActivity);
            PendingIntent pendingIntent = TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(startEditNoteActivity)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setContentIntent(pendingIntent);
            startActivity(startEditNoteActivity);


            return true;
        }
        if (id == R.id.item_detail_delete_note) {
            Intent startMainActivity = new Intent(this, MainActivity.class);
            if (deleteNote(_id)) {
                Toast.makeText(this, "note deleted", Toast.LENGTH_SHORT).show();
                startActivity(startMainActivity);
            } else {
                Toast.makeText(this, "something went wrong", Toast.LENGTH_SHORT).show();
                startActivity(startMainActivity);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);


        AuthStateHandler authStateHandler = new AuthStateHandler(this);

        if (!authStateHandler.isAuthenticated()) {
            Intent startLoginActivity = new Intent(this, LoginActivity.class);
            startActivity(startLoginActivity);
        }

        TextView note_body_tv = (TextView) findViewById(R.id.note_body_text_view);

        Intent intent = getIntent();
        if (intent != null) {

            title = intent.getStringArrayListExtra("data").get(0);
            body = intent.getStringArrayListExtra("data").get(1);
            _id = intent.getStringArrayListExtra("data").get(2);
        }

        this.setTitle(title);
        note_body_tv.setText(String.valueOf(body));
        if (findViewById(R.id.note_detail_container) != null) {
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.note_detail_container, new DetailFragment())
                        .commit();
            }
        }
    }

    private boolean deleteNote(String id) {
        JobScheduleUtil.scheduleJob(getBaseContext());
        return NoteListDbHelper.deleteNoteById(id, mDb);
    }

}
