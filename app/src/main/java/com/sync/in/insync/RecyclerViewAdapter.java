package com.sync.in.insync;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sync.in.insync.data.NoteListContract;
import com.sync.in.insync.data.NoteListDbHelper;
import com.sync.in.insync.utils.JobScheduleUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/18/2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    Context mContext;
    List<Note> mData;
    Cursor mCursor;

//    public RecyclerViewAdapter(Context mContext, List<Note> mData) {
//        this.mContext = mContext;
//        this.mData = mData;
//    }
    public RecyclerViewAdapter(Context mContext, Cursor cursor) {
        this.mContext = mContext;
        this.mCursor = cursor;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.item_note, parent, false);
        MyViewHolder vHolder = new MyViewHolder(v);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (!mCursor.moveToPosition(position)) {
            return;
        }
        final int pos = position;
        holder.note_title.setText(mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_TITLE)));
        holder.note_excerpt.setText(mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_EXCERPT)));
        holder.note_options_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(mContext, R.style.MyPopupMenu);
                PopupMenu popupMenu = new PopupMenu(wrapper, holder.note_options_tv);
//                PopupMenu popupMenu = new PopupMenu(mContext,  holder.note_options_tv);
                popupMenu.inflate(R.menu.note_options);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId()) {
                            case R.id.item_note_edit_menu:
                                mCursor.moveToPosition(holder.getLayoutPosition());
                                Intent startEditNoteActivity =
                                        new Intent(mContext, EditNoteActivity.class);
                                startEditNoteActivity.putExtra("_id",
                                        mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry._ID)));
                                startEditNoteActivity.putExtra("position", mCursor.getPosition());
                                PendingIntent pendingIntent = TaskStackBuilder.create(mContext)
                                        .addNextIntentWithParentStack(startEditNoteActivity)
                                        .getPendingIntent(0,
                                                PendingIntent.FLAG_UPDATE_CURRENT);
                                NotificationCompat.Builder builder =
                                        new NotificationCompat.Builder(mContext);
                                builder.setContentIntent(pendingIntent);
                                mContext.startActivity(startEditNoteActivity);
                                break;
                            case R.id.item_note_remove_menu:
                                mCursor.moveToPosition(holder.getLayoutPosition());
                                String _id = mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry._ID));
                                NoteListDbHelper dbHelper = new NoteListDbHelper(mContext);
                                NoteListDbHelper.deleteNoteById(_id, dbHelper.getWritableDatabase());
                                Toast.makeText(mContext, ""+position, Toast.LENGTH_SHORT).show();
                                notifyDataSetChanged();

                                JobScheduleUtil.scheduleJob(mContext);


                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
//        return mData.size();
        return mCursor.getCount();
    }

    public void swapCursor(Cursor cursor){
        if (cursor != null){
            mCursor.close();
        }
        mCursor = cursor;
        if (cursor!= null){
            this.notifyDataSetChanged();
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView note_title;
        private TextView note_excerpt;
        private TextView note_options_tv;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent noteDetailsIntent = new Intent(v.getContext(), NoteDetailActivity.class);
                    noteDetailsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    List<String> data = new ArrayList<String>();
                    if (!mCursor.moveToPosition(getLayoutPosition())) {
                        return;
                    }

                    data.add(mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_TITLE)));
                    data.add(mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_BODY)));
                    data.add(mCursor.getString(mCursor.getColumnIndex(NoteListContract.NoteListEntry._ID)));

                    noteDetailsIntent.putStringArrayListExtra("data", (ArrayList<String>) data);
                    PendingIntent pendingIntent = TaskStackBuilder.create(v.getContext())
                            .addNextIntentWithParentStack(noteDetailsIntent)
                            .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(v.getContext());
                    builder.setContentIntent(pendingIntent);
                    v.getContext().startActivity(noteDetailsIntent);
                }
            });

            note_title = (TextView) itemView.findViewById(R.id.item_note_title);
            note_excerpt = (TextView) itemView.findViewById(R.id.item_note_excerpt);
            note_options_tv = (TextView) itemView.findViewById(R.id.item_note_options_tv);
        }
    }
}
