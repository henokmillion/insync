package com.sync.in.insync.data;

import android.provider.BaseColumns;

/**
 * Created by User on 5/28/2018.
 */

public class NoteListContract {
    private NoteListContract() {
    }

    public static final class NoteListEntry implements BaseColumns {
        public static final String TABLE_NAME = "noteList";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_BODY = "body";
        public static final String COLUMN_EXCERPT = "excerpt";
        public static final String STATUS = "status";
        public static final String REMOTE_ID = "remote_id";
    }
}
