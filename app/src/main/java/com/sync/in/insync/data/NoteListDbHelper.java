package com.sync.in.insync.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.text.Editable;
import android.util.Log;

import com.sync.in.insync.utils.StringUtil;

import java.util.Arrays;
import java.util.List;

/**
 * Created by User on 5/28/2018.
 */

public class NoteListDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "notelist.db";
    private static int DATABASE_VERSION = 1;
    private static SQLiteDatabase mDb;
    private static Context mContext;

    public NoteListDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_NOTE_LIST_TABLE  = "CREATE TABLE " +
                NoteListContract.NoteListEntry.TABLE_NAME + " (" +
                NoteListContract.NoteListEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NoteListContract.NoteListEntry.REMOTE_ID + " VARCHAR(100) DEFAULT '0', " +
                NoteListContract.NoteListEntry.COLUMN_TITLE + " VARCHAR(40) NOT NULL, " +
                NoteListContract.NoteListEntry.COLUMN_EXCERPT + " TEXT NOT NULL, " +
                NoteListContract.NoteListEntry.COLUMN_BODY + " TEXT NOT NULL," +
                NoteListContract.NoteListEntry.STATUS + " VARCHAR(40) NOT NULL DEFAULT '"+
                NoteStatus.PENDING+"'" +");";
        db.execSQL(SQL_CREATE_NOTE_LIST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NoteListContract.NoteListEntry.TABLE_NAME);
        onCreate(db);
    }

    public static ContentValues addNote(String title, String body) {
        ContentValues cv = new ContentValues();
        cv.put(NoteListContract.NoteListEntry.COLUMN_TITLE, title);
        cv.put(NoteListContract.NoteListEntry.COLUMN_BODY, body);
        cv.put(NoteListContract.NoteListEntry.COLUMN_EXCERPT, StringUtil.getExcerpt(body));
        Log.i("ADD NOTE---", cv.toString());
        return cv;
    }

    public static ContentValues addSyncedNote(String title, String body,String excerpt, String rmid) {
        ContentValues cv = new ContentValues();
        cv.put(NoteListContract.NoteListEntry.COLUMN_TITLE, title);
        cv.put(NoteListContract.NoteListEntry.COLUMN_BODY, body);
        cv.put(NoteListContract.NoteListEntry.COLUMN_EXCERPT, excerpt);
        cv.put(NoteListContract.NoteListEntry.REMOTE_ID, rmid);
        cv.put(NoteListContract.NoteListEntry.STATUS, NoteStatus.SYNCED);
        return cv;
    }

    public static ContentValues updateNote(String title, String body, String excerpt) {
        ContentValues cv = new ContentValues();
        cv.put(NoteListContract.NoteListEntry.COLUMN_TITLE, title);
        cv.put(NoteListContract.NoteListEntry.COLUMN_BODY, body);
        cv.put(NoteListContract.NoteListEntry.COLUMN_EXCERPT, excerpt);

        return cv;
    }

    public static ContentValues updateNote(Editable title, Editable body, String excerpt) {
        ContentValues cv = new ContentValues();
        cv.put(NoteListContract.NoteListEntry.COLUMN_TITLE, title.toString());
        cv.put(NoteListContract.NoteListEntry.COLUMN_BODY, body.toString());
        cv.put(NoteListContract.NoteListEntry.COLUMN_EXCERPT, StringUtil.getExcerpt(body.toString()));
        cv.put(NoteListContract.NoteListEntry.STATUS, NoteStatus.UPDATED);
        return cv;
    }

    public static boolean deleteNoteById(String id, SQLiteDatabase db) {
        mDb = db;
        ContentValues cv = new ContentValues();
        cv.put(NoteListContract.NoteListEntry.STATUS, NoteStatus.DELETED);
        return mDb.update(NoteListContract.NoteListEntry.TABLE_NAME,
                cv,
                NoteListContract.NoteListEntry._ID+"=?", new String[]{id}) > 0;
    }

    public static class NoteStatus {
        public static String DELETED = "deleted";
        public static String PENDING = "pending";
        public static String SYNCED = "synced";
        public static String UPDATED = "updated";
    }


    public  static void updateVersion() {
        DATABASE_VERSION++;
    }

}
