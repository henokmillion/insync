package com.sync.in.insync.models;

public class User {
    private String fname;
    private String lname;
    private String birthDate;
    private String email;
    private String password;
    private String avatarUrl = "";

    public User(String fname, String lname, String birthDate, String email, String password,
                String avatarUrl) {
        this.fname = fname;
        this.lname = lname;
        this.birthDate = birthDate;
        this.email = email;
        this.password = password;
        this.avatarUrl = avatarUrl;
    }
}
