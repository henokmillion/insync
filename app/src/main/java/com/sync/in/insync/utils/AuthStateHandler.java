package com.sync.in.insync.utils;

import android.content.Context;
import android.widget.Toast;

public class AuthStateHandler {
    public static final String SHARED_PREFERENCES = "INSYNC_PREFERENCES";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String AUTH_STATE = "AUTH_STATE";
    private Context mContext;

    public AuthStateHandler(Context mContext) {
        this.mContext = mContext;
    }

    public boolean isAuthenticated() {
        return this.mContext.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getBoolean(AUTH_STATE, false);
    }

    public String getAuthToken() {

        return this.mContext.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getString(AUTH_TOKEN, "");
    }
}
