package com.sync.in.insync.utils;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

public class JobScheduleUtil {

    private static int JOB_ID = 1000;
    private static final String TAG = "JOB_SCHEDULER: ";

    public static void scheduleJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, NetworkJobService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            JobInfo jobInfo = new JobInfo.Builder(JOB_ID, serviceComponent)
                    .setPersisted(true)
                    .setPeriodic(15 * 60 * 1000)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .build();

            JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
            int resultCode = jobScheduler.schedule(jobInfo);
            if (resultCode == JobScheduler.RESULT_SUCCESS) {
                Log.i(TAG, "Job Scheduled");
            } else {
                Log.i(TAG, "Job Scheduling failed");
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void cancelJob(Context context) {
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        if (jobScheduler != null) {
            jobScheduler.cancel(JOB_ID);
            Log.i(TAG, "Job cancelled");
        }

    }
}
