package com.sync.in.insync.utils;

import android.annotation.SuppressLint;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.sync.in.insync.MainActivity;
import com.sync.in.insync.Note;
import com.sync.in.insync.RecyclerViewAdapter;
import com.sync.in.insync.data.NoteListContract;
import com.sync.in.insync.data.NoteListDbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class NetworkJobService extends JobService {
    private static final String TAG = "Job Scheduler: ";
    private static boolean jobCancelled = false;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        commitChanges(jobParameters);
        return true;
    }

    public void commitChanges(final JobParameters jobParameters) {
        DbQueryTask syncTask = new DbQueryTask(jobParameters, getBaseContext());
        syncTask.execute((Void) null);
        jobFinished(jobParameters, false);
    }

    static class SyncTask extends AsyncTask<Void, Void, String> {

        private static final String TAG = "Job Scheduler:";
        private JobParameters jobParameters;
        @SuppressLint("StaticFieldLeak")
        private Context mContext;

        public SyncTask(JobParameters jobParameters, Context baseContext) {
            this.jobParameters = jobParameters;
            this.mContext = baseContext;
        }

        private boolean isConnected() {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = null;
            if (connectivityManager != null) {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            } else {
                return false;
            }
            return networkInfo != null && networkInfo.isConnected();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                HttpURLConnection connection =
                        (HttpURLConnection) new URL(NetworkUtil.ApiUrl.NOTE_URL).openConnection();

                String token = mContext.getSharedPreferences(AuthStateHandler.SHARED_PREFERENCES,
                        mContext.MODE_PRIVATE).getString(AuthStateHandler.AUTH_TOKEN, "");
//                Log.i(TAG, token);
                connection.setRequestProperty("authorization", "Bearer " + token);
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                InputStream inputStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream, "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                inputStream.close();
                bufferedReader.close();
                connection.disconnect();
                return sb.toString().trim();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String response) {
//            NoteListDbHelper.updateVersion();
            NoteListDbHelper dbHelper = new NoteListDbHelper(mContext);
            dbHelper.onUpgrade(dbHelper.getWritableDatabase(), 1, 1);

            SQLiteDatabase mDb = dbHelper.getWritableDatabase();
            if (response != null) {
                Log.i(TAG, response);
                try {
                    JSONObject res = new JSONObject(response);
                    JSONArray notes = res.getJSONArray("data");
                    for (int i=0; i < notes.length(); i++) {
                        JSONObject note = notes.getJSONObject(i);
                        String title = note.getString("title");
                        String body = note.getString("body");
                        String remote_id = note.getString("_id");
                        String excerpt = note.getString("excerpt");
                        mDb.insert(NoteListContract.NoteListEntry.TABLE_NAME, null,
                                NoteListDbHelper.addSyncedNote(title, body,excerpt,  remote_id));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    mDb.close();
                    MainActivity.FetchFromDB fetchDataTask = new MainActivity.FetchFromDB(mContext);
                    fetchDataTask.execute((Void) null);
                }
            } else {
                Log.i(TAG, "null response");
            }
        }
    }

    static class CommitTask extends AsyncTask<Void, Void, String> {
        ArrayList<JSONObject> notes;
        Context context;
        JobParameters mjobParameters;

        public CommitTask(JobParameters jobParameters ,Context c,ArrayList <JSONObject> notes) {
            this.notes = notes;
            this.context = c;
            mjobParameters = jobParameters;
        }
        @Override
        protected void onPostExecute(String res) {
            if (res != null) {
                Log.i(TAG, res);
                new SyncTask(mjobParameters, context).execute();
            } else {
                new SyncTask(mjobParameters, context).execute();
                Log.i(TAG, "NULL POST/PUT/DELETE");
            }
        }

        @Override
        protected String doInBackground(Void... voids) {
            StringBuilder res = new StringBuilder();
            try {
                for (JSONObject jo: notes) {
                    Log.i(TAG, "jo----"+jo.toString());
                    String status = jo.getString("status");
                    JSONObject postdata = new JSONObject();
                    postdata.put("id", jo.getString("rmid"));

                    URL url = new URL(NetworkUtil.ApiUrl.NOTE_URL);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    String token = context.getSharedPreferences(AuthStateHandler.SHARED_PREFERENCES,
                            context.MODE_PRIVATE).getString(AuthStateHandler.AUTH_TOKEN, "");
                    connection.setRequestProperty("authorization", "Bearer " + token);
                    connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    if (status.equals(NoteListDbHelper.NoteStatus.UPDATED)) {
                        String route = NetworkUtil.ApiUrl.NOTE_URL + "/" + jo.getString("rmid");
                        url = new URL(route);
                        JSONObject note = new JSONObject();
                        connection = (HttpURLConnection)url.openConnection();
                        connection.setRequestMethod("PUT");
                        connection.setRequestProperty("authorization", "Bearer " + token);
                        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                        postdata.put("body", jo.getString("body"));
                        postdata.put("title", jo.getString("title"));
                        postdata.put("excerpt", jo.getString("excerpt"));
                        postdata.remove("id");
                        note.put("note", postdata);

                        connection.connect();

                        OutputStream outputStream = connection.getOutputStream();
                        outputStream.write(note.toString().getBytes("UTF-8"));

                        int stat = connection.getResponseCode();
                        Log.i(TAG, "UPDATED: " + note.toString());
                        Log.i(TAG, route);
                        Log.i(TAG, "STAT: " + stat);
//                        outputStream.close();
//                        connection.disconnect();
                    } else if (status.equals(NoteListDbHelper.NoteStatus.PENDING)) {
                        connection.setDoOutput(true);
                        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        postdata.put("body", jo.getString("body"));
                        postdata.put("title", jo.getString("title"));
                        postdata.remove("id");

                        OutputStream outputStream = connection.getOutputStream();
                        outputStream.write(postdata.toString().getBytes("UTF-8"));
//                        outputStream.close();

                        Log.i(TAG, "PENDING: " + res.toString());
                        int stat = connection.getResponseCode();
                        Log.i(TAG, "STAT: " + stat);
                        connection.disconnect();

                    } else if (status.equals(NoteListDbHelper.NoteStatus.DELETED)) {
                        String route = NetworkUtil.ApiUrl.NOTE_URL + "/" + jo.getString("rmid");
                        url = new URL(route);
                        connection = (HttpURLConnection)url.openConnection();
                        connection.setRequestProperty("authorization", "Bearer " + token);
                        connection.setRequestMethod("DELETE");
//                        connection.setRequestProperty("id", jo.getString("rmid"));
                        connection.connect();
//                        outputStream.close();

                        Log.i(TAG, "DELETED: " + jo.getString("rmid") + "--" +
                        jo.getString("title"));
                        int stat = connection.getResponseCode();
                        Log.i(TAG, "STAT: " + stat + postdata.getString("id"));
                        connection.disconnect();
                    }
//                    InputStream inputStream = connection.getInputStream();
//                    BufferedReader bufferedReader = new BufferedReader(
//                            new InputStreamReader(inputStream, "UTF-8"));
//                    String line = "";
//                    StringBuilder sb = new StringBuilder();
//                    while ((line = bufferedReader.readLine()) != null) {
//                        sb.append(line + "\n");
//                    }
//                    Log.i(TAG, sb.toString());
//                    inputStream.close();
//                    connection.disconnect();
//                    res.append(sb.toString());
                    connection.disconnect();
                }
//                Log.i(TAG, res.toString());
                return res.toString();
            } catch (Exception e) {

                e.printStackTrace();
            }
            return null;
        }
    }

    static class DbQueryTask extends AsyncTask<Void, Void, ArrayList<JSONObject>> {

        private SQLiteDatabase mDb;
        private Cursor mCursor;
        private Context mContext;
        private JobParameters mjobParameters;

        public DbQueryTask(JobParameters jobParameters, Context mContext) {
            this.mContext = mContext;
            this.mjobParameters = jobParameters;
        }

        @Override
        protected ArrayList<JSONObject> doInBackground(Void... voids) {
            ArrayList<JSONObject> syncNotes = new ArrayList<>();
            NoteListDbHelper dbHelper = new NoteListDbHelper(mContext);
            mDb = dbHelper.getWritableDatabase();
            mCursor = mDb.query(NoteListContract.NoteListEntry.TABLE_NAME, null, null,
                    null, null, null, null);
            List<String> rows = new ArrayList<>();
            Log.i(TAG, "row count" + mCursor.getCount());
            for (int i = 0; i < mCursor.getCount(); i++) {
                if (mCursor.moveToPosition(i)) {
//                    if (jobCancelled) {
//                        return null;
//                    }
                    JSONObject temp = new JSONObject();

                    String status = mCursor.getString(
                            mCursor.getColumnIndex(NoteListContract.NoteListEntry.STATUS));
                    Log.i(TAG, "STATUS: " + status);
                    String remote_id = mCursor.getString(
                            mCursor.getColumnIndex(NoteListContract.NoteListEntry.REMOTE_ID));
                    String body = mCursor.getString(
                            mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_BODY));
                    String title = mCursor.getString(
                            mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_TITLE));
                    String id = mCursor.getString(
                            mCursor.getColumnIndex(NoteListContract.NoteListEntry._ID));
                    String excerpt = mCursor.getString(
                            mCursor.getColumnIndex(NoteListContract.NoteListEntry.COLUMN_EXCERPT));

                    Log.i(TAG, id + "; --- " + title + "; --- " + remote_id +
                            "; ---- " + status);

                    if (status.equals(NoteListDbHelper.NoteStatus.SYNCED)) {
                        continue;
                    }
                    if (status.equals(NoteListDbHelper.NoteStatus.PENDING) ||
                    status.equals(NoteListDbHelper.NoteStatus.UPDATED)) {
                        try {
                            temp.put("body", body);
                            temp.put("title", title);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        temp.put("rmid", remote_id);
                        temp.put("status", status);
                        temp.put("body", body);
                        temp.put("title", title);
                        temp.put("excerpt", excerpt);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
//                        mDb.close();
                    }
                    syncNotes.add(temp);
                }
            }
            return syncNotes;
        }

        @Override
        protected void onPostExecute(ArrayList<JSONObject> notes) {
            if (notes != null) {
                Log.i(TAG, "DB RESPONSE: " + notes.toString());
            }
            new CommitTask(mjobParameters, mContext, notes).execute((Void) null);
        }
    }


    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.i(TAG, "Job cancelled before completion");
        jobCancelled = true;
        return true;
    }
}
