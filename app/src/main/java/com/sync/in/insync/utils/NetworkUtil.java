package com.sync.in.insync.utils;

public class NetworkUtil {
    private static String BASE_URL = "https://insync-api.herokuapp.com/";

    public static class ApiUrl {
        public static final String NOTE_URL = NetworkUtil.BASE_URL + "api/v1/note";
        public static final String USER_URL = NetworkUtil.BASE_URL + "api/v1/user";
        public static final String LOGIN_URL = NetworkUtil.BASE_URL + "api/v1/login";
    }
}
