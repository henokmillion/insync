package com.sync.in.insync.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StringUtil {
    public static JSONObject parseStringToJSON(String string) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(string);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
    public static String getExcerpt(String body) {
        int lenWords = body.length();
        String excerpt = body;
        if (lenWords > 30) {
            excerpt = body.substring(0, 30);
        }
        excerpt = excerpt.concat("...");
        return excerpt;

    }
}
