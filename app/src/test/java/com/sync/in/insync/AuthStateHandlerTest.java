package com.sync.in.insync;

import android.content.Context;
import android.content.SharedPreferences;

import com.sync.in.insync.utils.AuthStateHandler;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class AuthStateHandlerTest {

    private Context mockContext = mock(Context.class);
    private SharedPreferences mockSharedPrefernce = mock(SharedPreferences.class);
    private final AuthStateHandler authStateHandler = new AuthStateHandler(mockContext);
/*
    @Before
    public void before() throws Exception {
        mockSharedPrefernce = Mockito.mock(SharedPreferences.class);
        mockContext = Mockito.mock(Context.class);
        Mockito.when(mockContext.getSharedPreferences(anyString(), anyInt())).thenReturn(mockSharedPrefernce);
    }

    @Test
    public void testGetValidToken() throws Exception {
        Mockito.when(mockSharedPrefernce.getString(anyString(), anyString())).thenReturn("foobar");
        Assert.assertEquals("foobar", authStateHandler.getAuthToken());
        // maybe add some verify();
    }
*/

    @Test
    public void isAuthStateInstatiable() {
        assertNotNull(new AuthStateHandler(null));

    }

    @Test
    public void members_exists() throws Exception {
        assertNotNull(AuthStateHandler.AUTH_STATE);
        assertNotNull(AuthStateHandler.AUTH_TOKEN);
        assertNotNull(AuthStateHandler.SHARED_PREFERENCES);

    }

//    @Test
//    public void test_Wrong_Auth(){
//        String message = "is Wrong Authenthication Token";
//        SharedPreferences expected  = mockContext.getSharedPreferences(AuthStateHandler.SHARED_PREFERENCES,Context.MODE_PRIVATE);
//        String l = expected.getString(AuthStateHandler.AUTH_TOKEN," ");
//        String actual = "Fake Token Given Here To Test the Authentication stored in the shared Preference ";
//        Assert.assertNotSame(message, expected , actual  );
//    }
}
