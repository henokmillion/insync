package com.sync.in.insync;

import com.sync.in.insync.data.NoteListContract;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {


    @Test
    public void innerclass_exists() throws Exception {
        assertNotNull(new NoteListContract.NoteListEntry());
        System.out.println("innerclass_exists");
    }

    @Test
    public void innerclass_members() throws Exception {
        assertNotNull(new NoteListContract.NoteListEntry().TABLE_NAME);
        assertNotNull(new NoteListContract.NoteListEntry().COLUMN_BODY);
        assertNotNull(new NoteListContract.NoteListEntry().COLUMN_TITLE);
    }

    // test for NoteListDbHelper

}