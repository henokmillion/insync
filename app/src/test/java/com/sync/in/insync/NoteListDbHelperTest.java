package com.sync.in.insync;

import android.content.ContentValues;

import com.sync.in.insync.data.NoteListContract;
import com.sync.in.insync.data.NoteListDbHelper;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NoteListDbHelperTest {
    @Test
    public void test_addNote() {
        String[] input1 = {"title1", "the quick brown fox jumps over"};
        String[] input2 = {"title1", "the quick brown fox jumps over the lazy dog"};
        ContentValues expcected1 = new ContentValues();
        ContentValues expcected2 = new ContentValues();

        expcected1.put("title", input1[0]);
        expcected1.put("body", input1[1]);
        expcected1.put("excerpt", input1[1].substring(0, 30).concat("..."));

        expcected2.put("title", input2[0]);
        expcected2.put("body", input2[1]);
        expcected2.put("excerpt", input2[1].substring(0, 30).concat("..."));

       assertEquals(expcected1, NoteListDbHelper.addNote(input1[0], input1[1]));
        assertEquals(expcected2, NoteListDbHelper.addNote(input2[0], input2[1]));
    }

    @Test
    public void test_updateNote() {
        String[] input1 = {"title1", "the quick brown fox jumps over", "the quick brown fox jumps over"};
        String[] input2 = {"title1", "the quick brown fox jumps over the lazy dog", "the quick brown fox jumps over..."};
        ContentValues expcected1 = new ContentValues();
        ContentValues expcected2 = new ContentValues();

        expcected1.put("title", input1[0]);
        expcected1.put("body", input1[1]);
        expcected1.put("excerpt", input1[2]);

        expcected2.put("title", input2[0]);
        expcected2.put("body", input2[1]);
        expcected2.put("excerpt", input2[2]);

        assertEquals(expcected1, NoteListDbHelper.updateNote(input1[0], input1[1], input1[2]));
        assertEquals(expcected2, NoteListDbHelper.updateNote(input2[0], input2[1], input2[2]));
    }

    @Test
    public void test_addSyncedNote() {
        String[] input = {"title1", "the quick brown fox jumps over the lazy dog", "the quick brown fox jumps over...", "id"};

    }
}
