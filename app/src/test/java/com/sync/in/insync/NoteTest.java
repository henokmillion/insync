package com.sync.in.insync;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by eyoel on 6/22/18.
 */

public class NoteTest {
    private Note mockNote;
    private String Title;
    private String Body;
    private String Excerpt;

    @Before
    public void before() {
        Title = "Test Note";
        Body = "This is the body of the test note object.";
        Excerpt = "This is Excerpt";
        mockNote = new Note(Title, Excerpt, Body);
    }

    @Test
    public void testGetTitle(){
        Assert.assertEquals(Title,mockNote.getTitle());
    }

    @Test
    public void testGetExcerpt(){
        Assert.assertEquals(Excerpt,mockNote.getExcerpt());
    }

    @Test
    public void testGetBody(){
        Assert.assertEquals(Body,mockNote.getBody());
    }

    @Test
    public void testSetBody(){
        mockNote.setBody("New Body Set.");
        Assert.assertNotSame("Not equal", Body, mockNote.getBody() );

    }
}
