package com.sync.in.insync;

import com.sync.in.insync.utils.StringUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import  static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
// Test for
public class StringUtilTest {
    String inputValue;
    JSONObject expectedOutCome;
    @Before
    public void initaize() {

    }
    public StringUtilTest(String inputValue, JSONObject expectedOutCome) {
       this.inputValue = inputValue;
       this.expectedOutCome = expectedOutCome;
    }
    @Parameterized.Parameters
    public static Collection testCases() {
        String tc1, tc2, tc3;
        JSONObject expected1, expected2, expected3;
        expected1 = null;
        expected2 = null;
        expected3 = null;

        tc1 = "{title:\"Title 1\"}";
        tc2 = "{title:\"Title 1\", body: \"body\" }";
        tc3 = "{success: \"true 1\", data: \"[{title:\"Title 1\"}], {title:\"Title 2\"}]\"}";
        try {
            expected1 = new JSONObject(tc1);
            expected2 = new JSONObject(tc2);
            expected3 = new JSONObject(tc2);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Arrays.asList(new Object[][]{
                {tc1, expected1},
                {tc2, expected2},
                {tc3, expected3}
        });
    }
    @Test
    public void testparseStringToJSON() {
        assertEquals(this.inputValue, StringUtil.parseStringToJSON(this.inputValue));
    }


}
