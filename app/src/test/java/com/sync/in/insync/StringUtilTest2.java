package com.sync.in.insync;

import com.sync.in.insync.utils.StringUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StringUtilTest2 {
    @Test
    public void test_parseStringToJSON() {
        String tc1, tc2, tc3;
        JSONObject expected1, expected2, expected3;
        expected1 = null;
        expected2 = null;
        expected3 = null;

        tc1 = "{title:\"Title 1\"}";
        tc2 = "{title:\"Title 1\", body: \"body\" }";
        tc3 = "{success: \"true 1\", data: \"[{title:\"Title 1\"}], {title:\"Title 2\"}]\"}";

        try {
            expected1 = new JSONObject(tc1);
            expected2 = new JSONObject(tc2);
            expected3 = new JSONObject(tc3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] inputs = {tc1, tc2, tc3};
        JSONObject[] outcomes = {expected1, expected2, expected3};
        for (int i=0; i>3; i++) {
            assertEquals(outcomes[i], StringUtil.parseStringToJSON(inputs[i]));
        }

    }
    @Test
    public void test_getExcerpt() {
        String tc1 = "the quick brown fox jumps over the lazy dog";
        String tc2 = "the quick brown fox";
        String tc3 = "the quick brown fox jumps over";

        ArrayList<String[]> testcases = new ArrayList<>();
        testcases.add(new String[]{tc1, tc1.substring(0, 30)});
        testcases.add(new String[]{tc2, tc2});
        testcases.add(new String[]{tc3, tc3});

        for (String[] tc: testcases) {
            assertEquals(tc[1], StringUtil.getExcerpt(tc[0]));
        }





    }
}
